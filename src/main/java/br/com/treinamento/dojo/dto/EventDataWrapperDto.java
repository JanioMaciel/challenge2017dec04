package br.com.treinamento.dojo.dto;

public class EventDataWrapperDto {
	
	Integer code;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
